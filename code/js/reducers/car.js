export default function () {
  return [
    {
      id: 1,
      name: 'Audi',
      speed: 234,
      weight: 1.4,
      desc: 'r8',
      img: 'http://media.caranddriver.com/images/17q3/685272/2018-audi-r8-v10-rws-official-photos-and-info-news-car-and-driver-photo-690439-s-original.jpg'
    },
    {
      id: 2,
      name: 'BMW',
      speed: 334,
      weight: 2.4,
      desc: 'm3',
      img: 'https://upload.wikimedia.org/wikipedia/commons/e/e9/2016_BMW_M3_%28F80%29_sedan_%282017-11-18%29_01.jpg'
    },
    {
      id: 3,
      name: 'Mercedes',
      speed: 344,
      weight: 7.4,
      desc: 's-class',
      img: 'http://www.weddingcarsforhire.com/wp-content/uploads/2016/01/mercedes-sclass-hire-london_1.jpg'
    },
    {
      id: 4,
      name: 'Nissan',
      speed: 534,
      weight: 1.12,
      desc: 'Tiida',
      img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/2008_NISSAN_TIIDA_LATIO_rear.jpg/800px-2008_NISSAN_TIIDA_LATIO_rear.jpg'
    }
  ]
}
